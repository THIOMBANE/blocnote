package com.example.blocnote

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var displayText = ""
    val SETTINGSFILENAME = "com.example.blocnote"
    val KEYTEXT = "user saveText"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val prefReader = getSharedPreferences(SETTINGSFILENAME, Context.MODE_PRIVATE)
        displayText = prefReader.getString(KEYTEXT, "").toString()
        fullTextView.text = displayText
    }
        fun addButton(button:View) {
            val edit = editText.text
            displayText = "$displayText \n $edit"
            saveDataBase()
            fullTextView.text = displayText
            editText.text = null
    }

    fun saveDataBase() {
        val prefManager = getSharedPreferences(SETTINGSFILENAME, Context.MODE_PRIVATE).edit()
        prefManager.putString(KEYTEXT, displayText)
        prefManager.apply()
    }
}